# Takeout TODO

## MusicBrainz

* Prefer physical media over digital media or prefer releases with media that have titles

## Last.fm

* Scrobble tracks?

## Search

* Resulting ordering other than Bleve default?

## Music

* Full screen now playing view (web ui)
* Play history

## General

* Add users with UI (command line tool now supported)
* Improved web ui
* Use [Castjs](https://github.com/Fenny/Castjs) to cast media to TV, hub, etc.

## Dev work

* Refactor public/private usage
* Refactor use of pointers
* Consider Bluge vs Bleve

## Future

* Photos
* Alexa Skill
